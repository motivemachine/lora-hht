# LoRa HHT

A handheld transceiver for Lora SX1278 radios. Created to be a menu based controller/receiver for other lora projects and sensors. No code yet, still finishing the PCB design.

![front](LORA_HHT_front.png)
![back](LORA_HHT_back.png)
